<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xvg="http://xvergabe.org/notice/2.0" xmlns:bund="http://www.bund.de/importer/schema/ausschreibung/1.3" xmlns:xvgt="http://xvergabe.org/datatypes/1.0"
                xmlns:xvgbund="http://xvergabe.org/notice/bundde/1.3/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xvgwsdl="http://xvergabe.org/notice/wsdl/2.0/schema" exclude-result-prefixes="xvgt xvg xvgbund xvgwsdl">
	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="/">

		<!-- Tests am Anfang des Stylesheets um festzustellen, ob ein gültiges Ausgangsdokument für die Verwendung mit dem Proxy zur Weiterlieferung an Bund.de vorliegt -->
		<xsl:variable name="anzahlCAWs" select="count(/*/contractAward)"/>
		<xsl:variable name="cawDocumentTypes" as="xs:string+">
			<xsl:for-each select="/*/contractAward">
				<xsl:sequence select="local-name(./node())"/>
			</xsl:for-each>
		</xsl:variable>
		<!-- nur DE_CONTRACT_AWARD geliefert? -->
		<xsl:if test="count(distinct-values($cawDocumentTypes)) != 1">
			<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:differentDocumentTypes'),'Fehler: Die Lieferung enthält ungleiche Ex-Post-Dokumenttypen (nationale und EU). Es dürfen lediglich nationale ExPost-Dokumente geliefert werden.')"/>
		</xsl:if>
		<xsl:if test="distinct-values($cawDocumentTypes) != 'DE_CONTRACT_AWARD'">
			<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:wrongDocumentTypes'),'Fehler: Es dürfen lediglich nationale EX-Post Dokumente geliefert werden.')"/>
		</xsl:if>
		<xsl:variable name="lfrProviderID" as="xs:string+">
			<xsl:for-each select="/*/contractAward">
				<xsl:sequence select="./xvg:DE_CONTRACT_AWARD/xvg:DETAIL_PUBLICATION/xvgt:PROVIDER_ID"/>
			</xsl:for-each>
		</xsl:variable>
		<!-- alle CAWs mit selber LieferandenID? -->
		<xsl:if test="count(distinct-values($lfrProviderID)) != 1">
			<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:differentProviderIDs'),'Fehler: es dürfen pro Transaktion nur Datensätze eines einzigen Providers geliefert werden (PROVIDER_ID muss in allen Sätzen übereinstimmen)!')"/>
		</xsl:if>

		<!-- alle CAWs mit unterschiedlichem Deeplink? -->
		<xsl:variable name="lfrDeeplinks" as="xs:string+">
			<xsl:for-each select="/*/contractAward">
				<xsl:sequence select="./xvg:DE_CONTRACT_AWARD/xvg:DETAIL_PUBLICATION/xvgt:DEEPLINK_URI"/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:if test="count(distinct-values($lfrDeeplinks)) != $anzahlCAWs">
			<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:nonUniqueDeeplinkURIs'),'Fehler: Alle Lieferungen müssen eine eindeutige Deeplink-URI besitzen!')"/>
		</xsl:if>

		<!-- alle ProcedureIDs eindeutig? -->
		<xsl:variable name="procIDs" as="xs:string+">
			<xsl:for-each select="/*/contractAward">
				<xsl:sequence select="./xvg:DE_CONTRACT_AWARD/xvg:DETAIL_PUBLICATION/xvgt:PROCEDURE_ID"/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:if test="count(distinct-values($procIDs)) != $anzahlCAWs">
			<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:nonUniqueProcedureIDs'),'Fehler: Alle Lieferungen müssen eine eindeutige Procedure ID besitzen!')"/>
		</xsl:if>

		<!-- Alle ProcedureTypes national? -->
		<xsl:for-each select="/*/contractAward">
			<xsl:variable name="procType" select="./xvg:DE_CONTRACT_AWARD/xvg:TYPE_OF_PROCEDURE/code"/>
			<xsl:if test="$procType = 'OPEN_PROCEDURE' or $procType = 'NEGOTIATED_PROCEDURE' or $procType = 'RESTRICTED_PROCEDURE'">
				<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:nonNationalProcedureType'),'Fehler: Für mindestens eine Lieferung wurde eine europäische Verfahrensart angegeben.')"/>
			</xsl:if>
		</xsl:for-each>

		
		<bund:Lieferung>
			<bund:Steuerungsdaten>
				<bund:Lieferant>
					<xsl:value-of select="distinct-values($lfrProviderID)"/>
				</bund:Lieferant>
				<bund:Lieferart>
					<xsl:text>G</xsl:text>
				</bund:Lieferart>
				<bund:Lieferdatum>
					<xsl:value-of select="current-dateTime()"/>
				</bund:Lieferdatum>
				<bund:Liefernummer>
					<!-- Liefernummer wird zusammengesetzt aus den Zahlenwerten des aktuellen Datums/Uhrzeit: YYYYMMDDHHMMSS(OOoo)? -->
					<xsl:value-of select="replace(string(current-dateTime()),'[TZ:\.\-\+]','')"/>
				</bund:Liefernummer>
				<bund:Fachdatentyp>
					<xsl:text>Ausschreibung</xsl:text>
				</bund:Fachdatentyp>
			</bund:Steuerungsdaten>
			<xsl:for-each select="/*/contractAward/xvg:DE_CONTRACT_AWARD">
				<bund:Satz>
					<xsl:attribute name="id">
						<xsl:value-of select="./xvg:DETAIL_PUBLICATION/xvgt:PROCEDURE_ID"/>
					</xsl:attribute>
					<xsl:attribute name="kennzeichen">
						<xsl:text>I</xsl:text>
					</xsl:attribute>
					<bund:Metadaten>
						<bund:Erstellungsdatum>
							<xsl:value-of select="./xvg:DATE_OF_PUBLICATION"/>
						</bund:Erstellungsdatum>
						<bund:Archivierungsdatum>
							<xsl:value-of select="./xvg:DATE_OF_EXPIRATION"/>
						</bund:Archivierungsdatum>
					</bund:Metadaten>
					<bund:Daten>
						<bund:Titel>
							<xsl:value-of select="./xvg:SUBJECT_CONTRACT"/>
						</bund:Titel>
						<bund:Leistungsbeschreibung>
							<bund:div>
								<bund:p>
									<xsl:value-of select="./xvg:SUBJECT_DESCRIPTION"/>
								</bund:p>
							</bund:div>
						</bund:Leistungsbeschreibung>
						<bund:URL>
							<xsl:value-of select="./xvg:DETAIL_PUBLICATION/xvgt:DEEPLINK_URI"/>
						</bund:URL>
						<xsl:if test="exists(./xvg:COMMENT)">
							<bund:Bemerkung>
								<bund:div>
									<bund:p>
										<xsl:value-of select="./xvg:COMMENT"/>
									</bund:p>
								</bund:div>
							</bund:Bemerkung>
						</xsl:if>
						<xsl:if test="exists(./xvg:CPV)">
							<bund:CPV>
								<xsl:variable name="cpvcodes" as="xs:string+">
									<xsl:for-each select="./xvg:CPV">
										<xsl:sequence select="@CODE"/>
									</xsl:for-each>
								</xsl:variable>
								<xsl:value-of select="substring(string-join(distinct-values($cpvcodes), ', '), 1, 511)"/>
							</bund:CPV>
						</xsl:if>
						<bund:BeauftragtesUnternehmen>
							<xsl:value-of select="./xvg:CONTRACTOR/xvgt:NAME" />
							<xsl:if test="exists(./xvg:CONTRACTOR/xvgt:ADDRESS/*)">
								<xsl:if test="exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:STREET)">
									<xsl:text>
									</xsl:text>
									<xsl:value-of select="./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:STREET" />
									<xsl:if test="exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:STREET_NUMBER)">
										<xsl:value-of select="concat(' ',./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:STREET_NUMBER)" />
									</xsl:if>
								</xsl:if>
								<xsl:if test="not(exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:STREET)) and exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POST_OFFICE_BOX)">
									<xsl:text>
									</xsl:text>
									<xsl:value-of select="concat('Postfach: ', ./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POST_OFFICE_BOX)"/>
								</xsl:if>
								<xsl:if test="exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POSTAL_CODE) or exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POSTAL_CODE_POB)">
									<xsl:text>
									</xsl:text>
									<xsl:if test="exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POSTAL_CODE)">
										<xsl:value-of select="./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POSTAL_CODE"/>
									</xsl:if>
									<xsl:if test="not(exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POSTAL_CODE)) and exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POSTAL_CODE_POB)">
										<xsl:value-of select="./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POSTAL_CODE_POB"/>
									</xsl:if>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:if test="exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:CITY)">
									<xsl:if test="not(exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POSTAL_CODE) or exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:POSTAL_CODE_POB))">
										<xsl:text>
										</xsl:text>
									</xsl:if>
									<xsl:value-of select="./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:CITY" />
								</xsl:if>
								<xsl:if test="exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:FEDERAL_STATE)">
									<xsl:text>
									</xsl:text>
									<xsl:value-of select="./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:FEDERAL_STATE" />
								</xsl:if>
								<xsl:if test="exists(./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:COUNTRY)">
									<xsl:text>
									</xsl:text>
									<xsl:value-of select="./xvg:CONTRACTOR/xvgt:ADDRESS/xvgt:COUNTRY" />
								</xsl:if>
							</xsl:if>
						</bund:BeauftragtesUnternehmen>
						<xsl:if test="exists(./xvg:EXECUTION_INFORMATION/*)">
							<bund:ZeitraumDerAusfuehrung>
								<xsl:if test="exists(./xvg:EXECUTION_INFORMATION/xvgt:FROM)">
									<xsl:value-of select="concat('von: ',./xvg:EXECUTION_INFORMATION/xvgt:FROM)"/>
								</xsl:if>
								<xsl:if test="exists(./xvg:EXECUTION_INFORMATION/xvgt:UNTIL)">
									<xsl:if test="exists(./xvg:EXECUTION_INFORMATION/xvgt:FROM)">
										<xsl:text>; </xsl:text>
									</xsl:if>
									<xsl:value-of select="concat('bis: ', ./xvg:EXECUTION_INFORMATION/xvgt:UNTIL)"/>
								</xsl:if>
								<xsl:if test="exists(./xvg:EXECUTION_INFORMATION/xvgt:DURATION)">
									<xsl:if test="exists(./xvg:EXECUTION_INFORMATION/xvgt:FROM) or exists(./xvg:EXECUTION_INFORMATION/xvgt:UNTIL)">
										<xsl:text>; </xsl:text>
									</xsl:if>
									<xsl:value-of select="concat('Dauer: ', ./xvg:EXECUTION_INFORMATION/xvgt:DURATION)"/>
								</xsl:if>
							</bund:ZeitraumDerAusfuehrung>
						</xsl:if>
						<bund:Vergabestelle>
							<xsl:value-of select="substring(./xvg:CONTRACTING_AUTHORITY/xvgt:NAME, 1, 255)"/>
						</bund:Vergabestelle>
						<xsl:if test="exists(./xvg:KEYWORD)">
							<bund:Schluesselworte>
								<xsl:variable name="keywords" as="xs:string+">
									<xsl:for-each select="./xvg:KEYWORD">
										<xsl:sequence select="./text()"/>
									</xsl:for-each>
								</xsl:variable>
								<xsl:value-of select="substring(string-join(distinct-values($keywords), ', '), 1, 511)"/>
							</bund:Schluesselworte>
						</xsl:if>
						<bund:Kategorie>
							<xsl:variable name="category" select="./xvg:CATEGORY"/>
							<xsl:choose>
								<xsl:when test="$category='Arbeitsmarktdienstleistungen'">
									<xsl:value-of select="'Arbeitsmarktdienstleistungen'"/>
								</xsl:when>
								<xsl:when test="$category='Bauleistungen'">
									<xsl:value-of select="'Bauleistungen'"/>
								</xsl:when>
								<xsl:when test="$category='Bekleidung_Moebel_und_Druck'">
									<xsl:value-of select="'Bekleidung, Möbel und Druck'"/>
								</xsl:when>
								<xsl:when test="$category='Dienstleistungen'">
									<xsl:value-of select="'Dienstleistungen'"/>
								</xsl:when>
								<xsl:when test="$category='Energiequellen'">
									<xsl:value-of select="'Energiequellen'"/>
								</xsl:when>
								<xsl:when test="$category='Forschung_und_Entwicklung'">
									<xsl:value-of select="'Forschung und Entwicklung'"/>
								</xsl:when>
								<xsl:when test="$category='Informationstechnik'">
									<xsl:value-of select="'Informationstechnik'"/>
								</xsl:when>
								<xsl:when test="$category='Kommunikations_und_Elektrotechnik'">
									<xsl:value-of select="'Kommunikations- und Elektrotechnik'"/>
								</xsl:when>
								<xsl:when test="$category='Kraftfahrwesen'">
									<xsl:value-of select="'Kraftfahrwesen'"/>
								</xsl:when>
								<xsl:when test="$category='Lebensmittel'">
									<xsl:value-of select="'Lebensmittel'"/>
								</xsl:when>
								<xsl:when test="$category='Lieferleistungen'">
									<xsl:value-of select="'Lieferleistungen'"/>
								</xsl:when>
								<xsl:when test="$category='Maschinen'">
									<xsl:value-of select="'Maschinen'"/>
								</xsl:when>
								<xsl:when test="$category='Medizintechnik'">
									<xsl:value-of select="'Medizintechnik'"/>
								</xsl:when>
								<xsl:when test="$category='Metalle_Nichtmetalle'">
									<xsl:value-of select="'Metalle, Nichtmetalle'"/>
								</xsl:when>
								<xsl:when test="$category='Natuerliche_Erzeugnisse'">
									<xsl:value-of select="'Natürliche Erzeugnisse'"/>
								</xsl:when>
								<xsl:when test="$category='Rohwasser_Reinwasser'">
									<xsl:value-of select="'Rohwasser, Reinwasser'"/>
								</xsl:when>
								<xsl:when test="$category='Sanitaetswesen'">
									<xsl:value-of select="'Sanitätswesen'"/>
								</xsl:when>
								<xsl:when test="$category='Sekundaerrohstoffe'">
									<xsl:value-of select="'Sekundärrohstoffe'"/>
								</xsl:when>
								<xsl:when test="$category='Vorgefertigte_Erzeugnisse'">
									<xsl:value-of select="'Vorgefertigte Erzeugnisse'"/>
								</xsl:when>
								<xsl:when test="$category='Waffen_Munition_und_technische_Sondergeraete'">
									<xsl:value-of select="'Waffen, Munition und technische Sondergeräte'"/>
								</xsl:when>
							</xsl:choose>
						</bund:Kategorie>
						<bund:Verdingungsordnung>
							<xsl:variable name="contractingType" as="xs:string" select="./xvg:CONTRACTING_TYPE/code" />
							<xsl:choose>
								<xsl:when test="$contractingType = 'WORKS'">
									<xsl:text>VOB</xsl:text>
								</xsl:when>
								<xsl:when test="$contractingType = 'SERVICES'">
									<xsl:text>VOF</xsl:text>
								</xsl:when>
								<xsl:when test="$contractingType = 'SUPPLIES'">
									<xsl:text>VOL</xsl:text>
								</xsl:when>
								<xsl:when test="$contractingType = 'OTHER'">
									<xsl:text>sonstige</xsl:text>
								</xsl:when>
							</xsl:choose>
						</bund:Verdingungsordnung>
						<bund:Verfahrensart>
							<xsl:variable name="verfahrensArt" as="xs:string" select="./xvg:TYPE_OF_PROCEDURE/code" />
							<xsl:choose>
								<xsl:when test="$verfahrensArt = 'PUBLIC_TENDER'">
									<xsl:text>Öffentliche Ausschreibung</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'RESTRICTED_TENDER'">
									<xsl:text>Beschränkte Ausschreibung</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'RESTRICTED_TENDER_WITH_PARTICIPATION_CONTEST'">
									<xsl:text>Beschränkte Ausschreibung mit öffentlichem Teilnahmewettbewerb</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'SINGLE_TENDER_AUCTION'">
									<xsl:text>Freihändige Vergabe</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'SINGLE_TENDER_AUCTION_WITH_PARTICIPATION_CONTEST'">
									<xsl:text>Freihändige Vergabe mit öffentlichem Teilnahmewettbewerb</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:nonNationalProcedureType'),'Fehler: Für mindestens eine Lieferung wurde eine europäische Verfahrensart angegeben. Es dürfen lediglich Verfahrensarten unter dem Schwellenwert übermittelt werden (öffentliche Ausschreibung, beschränkte Ausschreibung (ggf. mit öffentlichem Teilnahmewettbewerb), freihändige Vergabe (ggf. mit öffentlichem Teilnahmewettbewerb)).')"/>
								</xsl:otherwise>
							</xsl:choose>
						</bund:Verfahrensart>
						<bund:Ausschreibungsweite>
							<!-- Ausschreibungsweite muss "national" sein, da der XVergabe-Proxy nur nationale EX-Post an bund.de liefern soll -->
							<xsl:text>National</xsl:text>
						</bund:Ausschreibungsweite>
						<bund:Ort>
							<xsl:if test="exists(./xvg:PLACE_OF_DELIVERY)">
								<!-- Wenn mehrere Orte angegeben wurden, werden diese mit ', ' miteinander verknüpft und anschließend auf 255 Zeichen getrimmt -->
								<xsl:variable name="cities" as="xs:string+">
									<xsl:for-each select="./xvg:PLACE_OF_DELIVERY">
										<xsl:sequence select="xvgbund:CITY/text()"/>
									</xsl:for-each>
								</xsl:variable>
								<xsl:value-of select="substring(string-join(distinct-values($cities), ', '), 1, 255)"/>
							</xsl:if>
						</bund:Ort>
						<xsl:if test="exists(./xvg:PLACE_OF_DELIVERY/xvgbund:POSTAL_CODE)">
							<!-- Wenn mehrere Postleitzahlen angegeben wurden, wird nur die erste PLZ genutzt -->
							<xsl:variable name="postleitzahlen" as="xs:string+">
								<xsl:for-each select="./xvg:PLACE_OF_DELIVERY">
									<xsl:sequence select="xvgbund:POSTAL_CODE/text()"/>
								</xsl:for-each>
							</xsl:variable>
							<bund:PLZ>
								<xsl:value-of select="distinct-values($postleitzahlen)[1]"/>
							</bund:PLZ>
						</xsl:if>
						<xsl:if test="exists(./xvg:PLACE_OF_DELIVERY/xvgbund:FEDERAL_STATE)">
							<!-- Wenn mehrere Bundesländer angegeben wurden als Ausführungsort, so wird nur eins im Zielschema angegeben -->
							<xsl:variable name="bundeslaender" as="xs:string+">
								<xsl:for-each select="./xvg:PLACE_OF_DELIVERY">
									<xsl:sequence select="xvgbund:FEDERAL_STATE/code/text()"/>
								</xsl:for-each>
							</xsl:variable>
							<xsl:variable name="bundesland" select="distinct-values($bundeslaender)[1]"/>
							<bund:Bundesland>
								<xsl:choose>
									<xsl:when test="$bundesland='BB'">
										<xsl:value-of select="'Brandenburg'"/>
									</xsl:when>
									<xsl:when test="$bundesland='BE'">
										<xsl:value-of select="'Berlin'"/>
									</xsl:when>
									<xsl:when test="$bundesland='BW'">
										<xsl:value-of select="'Baden-Württemberg'"/>
									</xsl:when>
									<xsl:when test="$bundesland='BY'">
										<xsl:value-of select="'Bayern'"/>
									</xsl:when>
									<xsl:when test="$bundesland='HB'">
										<xsl:value-of select="'Bremen'"/>
									</xsl:when>
									<xsl:when test="$bundesland='HE'">
										<xsl:value-of select="'Hessen'"/>
									</xsl:when>
									<xsl:when test="$bundesland='HH'">
										<xsl:value-of select="'Hamburg'"/>
									</xsl:when>
									<xsl:when test="$bundesland='MV'">
										<xsl:value-of select="'Mecklenburg-Vorpommern'"/>
									</xsl:when>
									<xsl:when test="$bundesland='NI'">
										<xsl:value-of select="'Niedersachsen'"/>
									</xsl:when>
									<xsl:when test="$bundesland='NW'">
										<xsl:value-of select="'Nordrhein-Westfalen'"/>
									</xsl:when>
									<xsl:when test="$bundesland='RP'">
										<xsl:value-of select="'Rheinland-Pfalz'"/>
									</xsl:when>
									<xsl:when test="$bundesland='SH'">
										<xsl:value-of select="'Schlesweig-Holstein'"/>
									</xsl:when>
									<xsl:when test="$bundesland='SL'">
										<xsl:value-of select="'Saarland'"/>
									</xsl:when>
									<xsl:when test="$bundesland='SN'">
										<xsl:value-of select="'Sachsen'"/>
									</xsl:when>
									<xsl:when test="$bundesland='ST'">
										<xsl:value-of select="'Sachsen-Anhalt'"/>
									</xsl:when>
									<xsl:when test="$bundesland='TH'">
										<xsl:value-of select="'Thüringen'"/>
									</xsl:when>
								</xsl:choose>
							</bund:Bundesland>
						</xsl:if>
					</bund:Daten>
				</bund:Satz>
			</xsl:for-each>
		</bund:Lieferung>
	</xsl:template>
</xsl:stylesheet>