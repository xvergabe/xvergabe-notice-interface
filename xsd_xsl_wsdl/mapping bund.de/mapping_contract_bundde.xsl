<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xvg="http://xvergabe.org/notice/2.0" xmlns:bund="http://www.bund.de/importer/schema/ausschreibung/1.3" xmlns:xvgt="http://xvergabe.org/datatypes/1.0"
                xmlns:xvgbund="http://xvergabe.org/notice/bundde/1.3/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xvgwsdl="http://xvergabe.org/notice/wsdl/2.0/schema" exclude-result-prefixes="xvgt xvg xvgbund xvgwsdl">
	<xsl:import-schema namespace="http://xvergabe.org/notice/bundde/1.3/1.0" schema-location="../bundde/xvergabe_bund_de_elements.xsd" />
	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="/">

		<xsl:for-each select="/*/contract">
			<!-- jede Lieferung muss einen RECEIVER_BUND_DE Teil besitzen-->
			<xsl:if test="not(exists(./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE))">
				<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:missingRECEIVER_BUND_DE_Content'),'Fehler: Es werden nur Lieferungen nationaler Bekanntmachungen zur Weiterleitung an bund.de angenommen, die ein RECEIVER_BUND_DE-Elemnt enhalten.')"/>
			</xsl:if>

			<!-- COMMON/CONTRACTING_TYPE muss gleich sein mit RECEIVER_BUND_DE/CONTRACTING_TYPE -->
			<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:CONTRACTING_TYPE)">
				<xsl:if test="./xvg:CONTRACT_COMMON/xvg:CONTRACTING_TYPE/code != ./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:CONTRACTING_TYPE/code">
					<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:missmatchingInformation'),'Fehler: Die Informationen zu CONTRACTING_TYPE in RECEIVER_BUND_DE und CONTRACT_COMMON stimmen in mindestens einer Lieferung nicht überein.')"/>	
				</xsl:if>
			</xsl:if>

			<!-- COMMON/PLACE_OF_DELIVERY muss gleich sein mit RECEIVER_BUND_DE/PLACE_OF_DELIVERY -->
			<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:PLACE_OF_DELIVERY)">
				<xsl:variable name="commonPoD" as="item()+" >
					<xsl:for-each select="./xvg:CONTRACT_COMMON/xvg:PLACE_OF_DELIVERY">
						<xsl:sequence select="." />
					</xsl:for-each>
				</xsl:variable>
				<xsl:variable name="bundPoD" as="item()+" >
					<xsl:for-each select="./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:PLACE_OF_DELIVERY" >
						<xsl:sequence select="." />
					</xsl:for-each>
				</xsl:variable>
				
				<xsl:if test="count($commonPoD) != count($bundPoD)">
					<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:missmatchingInformation'),'Fehler: Die Informationen zu PLACE_OF_DELIVERY in RECEIVER_BUND_DE und CONTRACT_COMMON stimmen in mindestens einer Lieferung nicht überein.')"/>	
				</xsl:if>

				<xsl:if test="not(deep-equal($commonPoD/*, $bundPoD/*))">
					<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:missmatchingInformation'),'Fehler: Die Informationen zu PLACE_OF_DELIVERY in RECEIVER_BUND_DE und CONTRACT_COMMON stimmen in mindestens einer Lieferung nicht überein.')"/>	
				</xsl:if>
			</xsl:if>
		</xsl:for-each>	

		<xsl:variable name="lfrProviderID" as="xs:string+">
			<xsl:for-each select="/*/contract">
				<xsl:sequence select="./xvg:CONTRACT_COMMON/xvg:DETAIL_PUBLICATION/xvgt:PROVIDER_ID"/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="lfrDeeplinks" as="xs:string+">
			<xsl:for-each select="/*/contract">
				<xsl:sequence select="./xvg:CONTRACT_COMMON/xvg:DETAIL_PUBLICATION/xvgt:DEEPLINK_URI"/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="lfrProcIDs" as="xs:string+">
			<xsl:for-each select="/*/contract">
				<xsl:sequence select="./xvg:CONTRACT_COMMON/xvg:DETAIL_PUBLICATION/xvgt:PROCEDURE_ID"/>
			</xsl:for-each>
		</xsl:variable>

			
		<!-- LieferantenID muss in allen Lieferungen gleich sein -->
		<xsl:if test="count(distinct-values($lfrProviderID)) != 1">
			<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:differentProviderIDs'),'Fehler: es dürfen pro Transaktion nur Datensätze eines einzigen Providers geliefert werden (PROVIDER_ID muss in allen Sätzen übereinstimmen)!')"/>
		</xsl:if>

		<!-- alle Lieferungen müssen unterschiedliche Deeplinks haben -->
		<xsl:if test="count(distinct-values($lfrDeeplinks)) != count(/*/contract)">
			<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:nonUniqueDeeplinkURIs'),'Fehler: Alle Lieferungen müssen eine eindeutige Deeplink-URI besitzen!')"/>
		</xsl:if>

		<!-- alle Lieferungen müssen unterschiedliche VerfahrensIDs besitzen -->
		<xsl:if test="count(distinct-values($lfrProcIDs)) != count(/*/contract)">
			<xsl:value-of select="error(QName('http://xvergabe.org/proxy','err:nonUniqueProcedureIDs'),'Fehler: Alle Lieferungen müssen eine eindeutige Procedure ID besitzen!')"/>
		</xsl:if>

		<bund:Lieferung>
			<bund:Steuerungsdaten>
				<bund:Lieferant>
					<xsl:value-of select="distinct-values($lfrProviderID)"/>
				</bund:Lieferant>
				<bund:Lieferart>
					<xsl:text>G</xsl:text>
				</bund:Lieferart>
				<bund:Lieferdatum>
					<xsl:value-of select="current-dateTime()"/>
				</bund:Lieferdatum>
				<bund:Liefernummer>
					<!-- Liefernummer wird zusammengesetzt aus den Zahlenwerten des aktuellen Datums/Uhrzeit: YYYYMMDDHHMMSS(OOoo)? -->
					<xsl:value-of select="replace(string(current-dateTime()),'[TZ:\.\-\+]','')"/>
				</bund:Liefernummer>
				<bund:Fachdatentyp>
					<xsl:text>Ausschreibung</xsl:text>
				</bund:Fachdatentyp>
			</bund:Steuerungsdaten>
			<xsl:for-each select="/*/contract">
				<bund:Satz>
					<xsl:attribute name="id">
						<xsl:value-of select="./xvg:CONTRACT_COMMON/xvg:DETAIL_PUBLICATION/xvgt:PROCEDURE_ID"/>
					</xsl:attribute>
					<xsl:attribute name="kennzeichen">
						<xsl:text>I</xsl:text>
					</xsl:attribute>
					<bund:Metadaten>
						<bund:Erstellungsdatum>
							<xsl:value-of select="./xvg:CONTRACT_COMMON/xvg:DATE_OF_PUBLICATION"/>
						</bund:Erstellungsdatum>
						<bund:Archivierungsdatum>
							<xsl:choose>
								<xsl:when test="exists(./xvg:CONTRACT_COMMON/xvg:DATE_OF_CANCELLATION)">
									<xsl:value-of select="./xvg:CONTRACT_COMMON/xvg:DATE_OF_CANCELLATION"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="exists(./xvg:CONTRACT_COMMON/xvg:DATE_OF_EXPIRATION)">
											<xsl:value-of select="./xvg:CONTRACT_COMMON/xvg:DATE_OF_EXPIRATION" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:TENDER_DUE_DATE" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</bund:Archivierungsdatum>
					</bund:Metadaten>
					<bund:Daten>
						<bund:Titel>
							<xsl:value-of select="./xvg:CONTRACT_COMMON/xvg:SUBJECT_CONTRACT"/>
						</bund:Titel>
						<bund:Leistungsbeschreibung>
							<bund:div>
								<bund:p>
									<xsl:value-of select="./xvg:CONTRACT_COMMON/xvg:SUBJECT_DESCRIPTION"/>
								</bund:p>
							</bund:div>
						</bund:Leistungsbeschreibung>
						<bund:URL>
							<xsl:value-of select="./xvg:CONTRACT_COMMON/xvg:DETAIL_PUBLICATION/xvgt:DEEPLINK_URI"/>
						</bund:URL>
						<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:COMMENT)">
							<bund:Bemerkung>
								<bund:div>
									<bund:p>
										<xsl:value-of select="./xvg:CONTRACT_COMMON/xvg:COMMENT"/>
									</bund:p>
								</bund:div>
							</bund:Bemerkung>
						</xsl:if>
						<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:CPV)">
							<bund:CPV>
								<xsl:variable name="cpvcodes" as="xs:string+">
									<xsl:for-each select="./xvg:CONTRACT_COMMON/xvg:CPV">
										<xsl:sequence select="@CODE"/>
									</xsl:for-each>
								</xsl:variable>
								<xsl:value-of select="substring(string-join(distinct-values($cpvcodes), ', '), 1, 511)"/>
							</bund:CPV>
						</xsl:if>						
						<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:EXECUTION_INFORMATION/*)">
							<bund:ZeitraumDerAusfuehrung>
								<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:EXECUTION_INFORMATION/xvgt:FROM)">
									<xsl:value-of select="concat('von: ',./xvg:CONTRACT_COMMON/xvg:EXECUTION_INFORMATION/xvgt:FROM)"/>
								</xsl:if>
								<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:EXECUTION_INFORMATION/xvgt:UNTIL)">
									<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:EXECUTION_INFORMATION/xvgt:FROM)">
										<xsl:text>; </xsl:text>
									</xsl:if>
									<xsl:value-of select="concat('bis: ', ./xvg:CONTRACT_COMMON/xvg:EXECUTION_INFORMATION/xvgt:UNTIL)"/>
								</xsl:if>
								<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:EXECUTION_INFORMATION/xvgt:DURATION)">
									<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:EXECUTION_INFORMATION/xvgt:FROM) or exists(./xvg:CONTRACT_COMMON/xvg:EXECUTION_INFORMATION/xvgt:UNTIL)">
										<xsl:text>; </xsl:text>
									</xsl:if>
									<xsl:value-of select="concat('Dauer: ', ./xvg:CONTRACT_COMMON/xvg:EXECUTION_INFORMATION/xvgt:DURATION)"/>
								</xsl:if>
							</bund:ZeitraumDerAusfuehrung>
						</xsl:if>
						<bund:Vergabestelle>
							<xsl:value-of select="./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:CONTRACTING_AUTHORITY_NAME"/>
						</bund:Vergabestelle>
						<xsl:if test="exists(./xvg:CONTRACT_COMMON/xvg:KEYWORD)">
							<bund:Schluesselworte>
								<xsl:variable name="keywords" as="xs:string+">
									<xsl:for-each select="./xvg:CONTRACT_COMMON/xvg:KEYWORD">
										<xsl:sequence select="./text()"/>
									</xsl:for-each>
								</xsl:variable>
								<xsl:value-of select="substring(string-join(distinct-values($keywords), ', '), 1, 511)"/>
							</bund:Schluesselworte>
						</xsl:if>
						<bund:Kategorie>
							<xsl:variable name="category" select="./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:CATEGORY"/>
							<xsl:choose>
								<xsl:when test="$category='Arbeitsmarktdienstleistungen'">
									<xsl:value-of select="'Arbeitsmarktdienstleistungen'"/>
								</xsl:when>
								<xsl:when test="$category='Bauleistungen'">
									<xsl:value-of select="'Bauleistungen'"/>
								</xsl:when>
								<xsl:when test="$category='Bekleidung_Moebel_und_Druck'">
									<xsl:value-of select="'Bekleidung, Möbel und Druck'"/>
								</xsl:when>
								<xsl:when test="$category='Dienstleistungen'">
									<xsl:value-of select="'Dienstleistungen'"/>
								</xsl:when>
								<xsl:when test="$category='Energiequellen'">
									<xsl:value-of select="'Energiequellen'"/>
								</xsl:when>
								<xsl:when test="$category='Forschung_und_Entwicklung'">
									<xsl:value-of select="'Forschung und Entwicklung'"/>
								</xsl:when>
								<xsl:when test="$category='Informationstechnik'">
									<xsl:value-of select="'Informationstechnik'"/>
								</xsl:when>
								<xsl:when test="$category='Kommunikations_und_Elektrotechnik'">
									<xsl:value-of select="'Kommunikations- und Elektrotechnik'"/>
								</xsl:when>
								<xsl:when test="$category='Kraftfahrwesen'">
									<xsl:value-of select="'Kraftfahrwesen'"/>
								</xsl:when>
								<xsl:when test="$category='Lebensmittel'">
									<xsl:value-of select="'Lebensmittel'"/>
								</xsl:when>
								<xsl:when test="$category='Lieferleistungen'">
									<xsl:value-of select="'Lieferleistungen'"/>
								</xsl:when>
								<xsl:when test="$category='Maschinen'">
									<xsl:value-of select="'Maschinen'"/>
								</xsl:when>
								<xsl:when test="$category='Medizintechnik'">
									<xsl:value-of select="'Medizintechnik'"/>
								</xsl:when>
								<xsl:when test="$category='Metalle_Nichtmetalle'">
									<xsl:value-of select="'Metalle, Nichtmetalle'"/>
								</xsl:when>
								<xsl:when test="$category='Natuerliche_Erzeugnisse'">
									<xsl:value-of select="'Natürliche Erzeugnisse'"/>
								</xsl:when>
								<xsl:when test="$category='Rohwasser_Reinwasser'">
									<xsl:value-of select="'Rohwasser, Reinwasser'"/>
								</xsl:when>
								<xsl:when test="$category='Sanitaetswesen'">
									<xsl:value-of select="'Sanitätswesen'"/>
								</xsl:when>
								<xsl:when test="$category='Sekundaerrohstoffe'">
									<xsl:value-of select="'Sekundärrohstoffe'"/>
								</xsl:when>
								<xsl:when test="$category='Vorgefertigte_Erzeugnisse'">
									<xsl:value-of select="'Vorgefertigte Erzeugnisse'"/>
								</xsl:when>
								<xsl:when test="$category='Waffen_Munition_und_technische_Sondergeraete'">
									<xsl:value-of select="'Waffen, Munition und technische Sondergeräte'"/>
								</xsl:when>
							</xsl:choose>
						</bund:Kategorie>
						<bund:Verdingungsordnung>
							<xsl:variable name="contractingType" as="xs:string" select="./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:CONTRACTING_TYPE/code" />
							<xsl:choose>
								<xsl:when test="$contractingType = 'WORKS'">
									<xsl:text>VOB</xsl:text>
								</xsl:when>
								<xsl:when test="$contractingType = 'SERVICES'">
									<xsl:text>VOF</xsl:text>
								</xsl:when>
								<xsl:when test="$contractingType = 'SUPPLIES'">
									<xsl:text>VOL</xsl:text>
								</xsl:when>
								<xsl:when test="$contractingType = 'OTHER'">
									<xsl:text>sonstige</xsl:text>
								</xsl:when>
							</xsl:choose>
						</bund:Verdingungsordnung>
						<bund:Verfahrensart>
							<xsl:variable name="verfahrensArt" as="xs:string" select="./xvg:CONTRACT_COMMON/xvg:TYPE_OF_PROCEDURE/code" />
							<xsl:choose>
								<xsl:when test="$verfahrensArt = 'PUBLIC_TENDER'">
									<xsl:text>Öffentliche Ausschreibung</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'RESTRICTED_TENDER'">
									<xsl:text>Beschränkte Ausschreibung</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'RESTRICTED_TENDER_WITH_PARTICIPATION_CONTEST'">
									<xsl:text>Beschränkte Ausschreibung mit öffentlichem Teilnahmewettbewerb</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'SINGLE_TENDER_AUCTION'">
									<xsl:text>Freihändige Vergabe</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'SINGLE_TENDER_AUCTION_WITH_PARTICIPATION_CONTEST'">
									<xsl:text>Freihändige Vergabe mit öffentlichem Teilnahmewettbewerb</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'OPEN_PROCEDURE'">
									<xsl:text>Offenes Verfahren</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'RESTRICTED_PROCEDURE'">
									<xsl:text>Nicht Offenes Verfahren mit öffentlichem Teilnahmewettbewerb</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'NEGOTIATED_PROCEDURE'">
									<xsl:text>Verhandlungsverfahren mit öffentlichem Teilnahmewettbewerb</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'COMPETITIVE_DIALOGUE'">
									<xsl:text>Wettbewerblicher Dialog</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'AUCTION_RESTRICTED'">
									<xsl:text>Elektronische Auktion</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'AUCTION_NOT_RESTRICTED'">
									<xsl:text>Elektronische Auktion</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'REQUEST_FOR_QUOTATION'">
									<xsl:text>Dynamisches elektronisches Verfahren</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'PUBLIC_REQUEST_FOR_QUOTATION'">
									<xsl:text>Dynamisches elektronisches Verfahren</xsl:text>
								</xsl:when>
								<xsl:when test="$verfahrensArt = 'EXPRESSION_OF_INTEREST'">
									<xsl:text>Interessenbekundungsverfahren</xsl:text>
								</xsl:when>
							</xsl:choose>
						</bund:Verfahrensart>
						<bund:Ausschreibungsweite>
							<xsl:variable name="scope" as="xs:string" select="./xvg:CONTRACT_COMMON/xvg:SCOPE_OF_PROCEDURE" />
							<xsl:choose>
								<xsl:when test="$scope = 'EU'">
									<xsl:text>EU-Weit</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$scope" />
								</xsl:otherwise>
							</xsl:choose>
						</bund:Ausschreibungsweite>
						<bund:Ort>
							<!-- Wenn mehrere Orte angegeben wurden, werden diese mit ', ' miteinander verknüpft und anschließend auf 255 Zeichen getrimmt -->
							<xsl:variable name="cities" as="xs:string+">
								<xsl:for-each select="./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:PLACE_OF_DELIVERY">
									<xsl:sequence select="xvgbund:CITY/text()"/>
								</xsl:for-each>
							</xsl:variable>
							<xsl:value-of select="substring(string-join(distinct-values($cities), ', '), 1, 255)"/>
						</bund:Ort>
						<xsl:if test="exists(./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvg:PLACE_OF_DELIVERY/xvgbund:POSTAL_CODE)">
							<!-- Wenn mehrere Postleitzahlen angegeben wurden, wird nur die erste PLZ genutzt -->
							<xsl:variable name="postleitzahlen" as="xs:string+">
								<xsl:for-each select="./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvg:PLACE_OF_DELIVERY">
									<xsl:if test="exists(./xvgbund:POSTAL_CODE)">
										<xsl:sequence select="xvgbund:POSTAL_CODE/text()"/>
									</xsl:if>
								</xsl:for-each>
							</xsl:variable>
							<bund:PLZ>
								<xsl:value-of select="distinct-values($postleitzahlen)[1]"/>
							</bund:PLZ>
						</xsl:if>
						<xsl:if test="exists(./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvg:PLACE_OF_DELIVERY/xvgbund:FEDERAL_STATE)">
							<!-- Wenn mehrere Bundesländer angegeben wurden als Ausführungsort, so wird nur eins im Zielschema angegeben -->
							<xsl:variable name="bundeslaender" as="xs:string+">
								<xsl:for-each select="./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvg:PLACE_OF_DELIVERY">
									<xsl:if test="exists(./xvgbund:FEDERAL_STATE)">
										<xsl:sequence select="xvgbund:FEDERAL_STATE/code"/>
									</xsl:if>
								</xsl:for-each>
							</xsl:variable>
							<xsl:variable name="bundesland" select="distinct-values($bundeslaender)[1]"/>
							<bund:Bundesland>
								<xsl:choose>
									<xsl:when test="$bundesland='BB'">
										<xsl:value-of select="'Brandenburg'"/>
									</xsl:when>
									<xsl:when test="$bundesland='BE'">
										<xsl:value-of select="'Berlin'"/>
									</xsl:when>
									<xsl:when test="$bundesland='BW'">
										<xsl:value-of select="'Baden-Württemberg'"/>
									</xsl:when>
									<xsl:when test="$bundesland='BY'">
										<xsl:value-of select="'Bayern'"/>
									</xsl:when>
									<xsl:when test="$bundesland='HB'">
										<xsl:value-of select="'Bremen'"/>
									</xsl:when>
									<xsl:when test="$bundesland='HE'">
										<xsl:value-of select="'Hessen'"/>
									</xsl:when>
									<xsl:when test="$bundesland='HH'">
										<xsl:value-of select="'Hamburg'"/>
									</xsl:when>
									<xsl:when test="$bundesland='MV'">
										<xsl:value-of select="'Mecklenburg-Vorpommern'"/>
									</xsl:when>
									<xsl:when test="$bundesland='NI'">
										<xsl:value-of select="'Niedersachsen'"/>
									</xsl:when>
									<xsl:when test="$bundesland='NW'">
										<xsl:value-of select="'Nordrhein-Westfalen'"/>
									</xsl:when>
									<xsl:when test="$bundesland='RP'">
										<xsl:value-of select="'Rheinland-Pfalz'"/>
									</xsl:when>
									<xsl:when test="$bundesland='SH'">
										<xsl:value-of select="'Schlesweig-Holstein'"/>
									</xsl:when>
									<xsl:when test="$bundesland='SL'">
										<xsl:value-of select="'Saarland'"/>
									</xsl:when>
									<xsl:when test="$bundesland='SN'">
										<xsl:value-of select="'Sachsen'"/>
									</xsl:when>
									<xsl:when test="$bundesland='ST'">
										<xsl:value-of select="'Sachsen-Anhalt'"/>
									</xsl:when>
									<xsl:when test="$bundesland='TH'">
										<xsl:value-of select="'Thüringen'"/>
									</xsl:when>
								</xsl:choose>
							</bund:Bundesland>
						</xsl:if>
						<xsl:if test="exists(./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:TIMELIMIT_REQUESTING_DOCUMENTS)">
							<bund:Anforderungsfrist>
								<xsl:value-of select="./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:TIMELIMIT_REQUESTING_DOCUMENTS" />
							</bund:Anforderungsfrist>
						</xsl:if>
						<xsl:if test="exists(./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:TENDER_DUE_DATE)">
							<bund:Angebotsfrist>
								<xsl:value-of select="./xvg:RECEIVER_SPECIFIC/xvg:RECEIVER_BUND_DE/xvgbund:TENDER_DUE_DATE" />
							</bund:Angebotsfrist>
						</xsl:if>
					</bund:Daten>
				</bund:Satz>
			</xsl:for-each>
		</bund:Lieferung>
	</xsl:template>
</xsl:stylesheet>